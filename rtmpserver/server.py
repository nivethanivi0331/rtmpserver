import asyncio

from rtmp_protocol import RTMPProtocol


class RTMPServer:
    def run(self):
        asyncio.run(self.main())

    async def main(self):
        print("Starting server")
        HOST, PORT = "127.0.0.1", 9999

        loop = asyncio.get_running_loop()
        server = await loop.create_server(RTMPProtocol, HOST, PORT)
        print('Serving on {}'.format(server.sockets[0].getsockname()))
        print('https://127.0.0.1/live')
        await server.serve_forever()

        # Close the server
        server.close()
        loop.run_until_complete(server.wait_closed())
        loop.close()

#server

